package com.pangaea.quests.viewModel;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.pangaea.quests.QuestsApplication;
import com.pangaea.quests.networkClient.QuestService;
import com.pangaea.quests.networkClient.requestObject.LoginObject;
import com.pangaea.quests.networkClient.responseObject.LoginResponse;
import com.pangaea.quests.networkClient.ServiceGenerator;
import com.pangaea.quests.networkClient.responseObject.UserData;
import com.pangaea.quests.utils.PreferenceUtils;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class LoginViewModel extends ViewModel {

//    private Observable<LoginResponse> mLoginResponseObservable;
//    private Observable<UserData> mUserResponseObservable;

    private Disposable mLoginDisposable;
    private Disposable mUserDisposable;
    QuestService mQuestService;
    private MutableLiveData<LiveDataLoginContainer> mLoadedLoginData;
    private MutableLiveData<LiveDataUserContainer> mLoadedUserData;

    public static LoginViewModel getViewModel(@NonNull FragmentActivity activity) {
        return ViewModelProviders.of(activity).get(LoginViewModel.class);
    }

    public LoginViewModel() {
        super();
        mQuestService = ServiceGenerator.createService(QuestService.class);

        Log.e("URL_U", mQuestService.getUserDataF().request().url().toString());   //todo remove

    }

    public LiveData<LiveDataLoginContainer> getLoginResponseLiveData() {
        if (mLoadedLoginData == null) {
            mLoadedLoginData = new MutableLiveData<LiveDataLoginContainer>();
        }
        return mLoadedLoginData;
    }

    public LiveData<LiveDataUserContainer> getUserResponseLiveData() {
        if (mLoadedUserData == null) {
            mLoadedUserData = new MutableLiveData<LiveDataUserContainer>();
        }
        return mLoadedUserData;
    }

    public void clearLoginLifeData() {
        mLoadedLoginData.setValue(null);
        safeDispose(mLoginDisposable);
    }

    public void clearUserLifeData() {
        mLoadedUserData.setValue(null);
        safeDispose(mUserDisposable);
    }


    public void runLoginSubscription(String userId, String userOauthToken) {
        safeDispose(mLoginDisposable);
//        String userId = PreferenceUtils.getUserId(QuestsApplication.get());
//        PreferenceUtils.getUserToken(QuestsApplication.get());
        Observable<LoginResponse> mLoginResponseObservable =
                mQuestService
                        .login(new LoginObject(userId, userOauthToken));
        mLoginDisposable = mLoginResponseObservable.subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())  //todo remove it?
                .subscribeWith(new DisposableObserver<LoginResponse>() {
                    @Override
                    public void onNext(LoginResponse value) {

                        Log.e("RXT", "onNext ");
                        mLoadedLoginData.postValue(new LiveDataLoginContainer(value, null));
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("RXT", "onError ");
                        mLoadedLoginData.postValue(new LiveDataLoginContainer(null, e));
                    }

                    @Override
                    public void onComplete() {
                        Log.e("RXT", "onComplete ");
                    }
                });
    }

    public void runUserDataSubscription() {

        safeDispose(mUserDisposable);

        Observable<UserData> mUserResponseObservable = mQuestService.getUserDetails();


        mUserDisposable = mUserResponseObservable.subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())  //todo remove it?
                .subscribeWith(new DisposableObserver<UserData>() {
                    @Override
                    public void onNext(UserData userData) {
                        Log.e("RXT_U", "onNext ");
                        Log.e("RXT_U", userData.toString());
                        mLoadedUserData.postValue(new LiveDataUserContainer(userData, null));
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("RXT_U", "onError ");
                        mLoadedUserData.postValue(new LiveDataUserContainer(null, e));
                    }

                    @Override
                    public void onComplete() {
                        Log.e("RXT_U", "onComplete ");
                    }
                });
    }


    private void safeDispose(Disposable disposable) {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    public static class LiveDataLoginContainer {

        private boolean noErrorResponse = true;
        private LoginResponse loginResponse;
        private Throwable throwable;


        public LiveDataLoginContainer(LoginResponse loginResponse, Throwable throwable) {
            this.loginResponse = loginResponse;
            if (throwable != null) {
                noErrorResponse = false;
                this.throwable = throwable;
            }
        }

        public LoginResponse getLoginResponse() {
            return loginResponse;
        }

        public Throwable getThrowable() {
            return throwable;
        }

        public boolean isNoErrorResponse() {
            return noErrorResponse;
        }
    }


    public static class LiveDataUserContainer {

        private boolean noErrorResponse = true;
        private UserData userData;
        private Throwable throwable;


        public LiveDataUserContainer(UserData userData, Throwable throwable) {
            this.userData = userData;
            if (throwable != null) {
                noErrorResponse = false;
                this.throwable = throwable;
            }
        }

        public UserData getUserData() {
            return userData;
        }

        public Throwable getThrowable() {
            return throwable;
        }

        public boolean isNoErrorResponse() {
            return noErrorResponse;
        }
    }
}
