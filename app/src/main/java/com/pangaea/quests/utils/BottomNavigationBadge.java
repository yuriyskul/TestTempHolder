package com.pangaea.quests.utils;

import android.content.Context;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.pangaea.quests.R;


public class BottomNavigationBadge {
    public final static int NOTIFICATION_TAB_POSITION = 3; //position of Tab
    public final static int NO_BUDGETS = 0;
    private final static int TOO_MANY_BADGES_TO_DISPLAY = 10;
    private final static String MANY_BUDGETS_TEXT = "9+";

    public static void setUpBadge(BottomNavigationView navigationView, int badgeCount, Context context) {
        //check if exist badge view{
        TextView badgeView = navigationView.findViewById(R.id.badge_tv);
        if (badgeView != null) {
            setCountToBadgeView(badgeView, badgeCount);
        } else {
            if (badgeCount == NO_BUDGETS) {
                return; // no need to add badge view cause we have no need to show badge recently
            }
            BottomNavigationMenuView bottomNavigationMenuView =
                    (BottomNavigationMenuView) navigationView.getChildAt(0);
            View v = bottomNavigationMenuView.getChildAt(NOTIFICATION_TAB_POSITION);
            BottomNavigationItemView itemView = (BottomNavigationItemView) v;
            View badge = LayoutInflater.from(context)
                    .inflate(R.layout.badge, bottomNavigationMenuView, false);
            badgeView = badge.findViewById(R.id.badge_tv);

            setCountToBadgeView(badgeView, badgeCount);
            itemView.addView(badge);
        }

    }

    private static void setCountToBadgeView(TextView badgeView, int badgeCount) {
        if (badgeCount == NO_BUDGETS) {
            badgeView.setVisibility(View.INVISIBLE);
        } else if (badgeCount > NO_BUDGETS && badgeCount < TOO_MANY_BADGES_TO_DISPLAY) {
            badgeView.setText(String.valueOf(badgeCount));
        }
        if (badgeCount >= TOO_MANY_BADGES_TO_DISPLAY)
            badgeView.setText(MANY_BUDGETS_TEXT);
    }


}
