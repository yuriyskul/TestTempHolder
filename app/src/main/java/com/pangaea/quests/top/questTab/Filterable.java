package com.pangaea.quests.top.questTab;

/**
 * Created by yuriy on 2/16/2018.
 */

public interface Filterable {
    void applyFilter(int filterType);
}
