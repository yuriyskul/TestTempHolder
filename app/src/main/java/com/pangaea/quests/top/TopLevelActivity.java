package com.pangaea.quests.top;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pangaea.quests.QuestsApplication;
import com.pangaea.quests.R;
import com.pangaea.quests.model.User;
import com.pangaea.quests.top.createTab.CreateQuestActivity;
import com.pangaea.quests.top.questTab.TopQuestsFragment;
import com.pangaea.quests.utils.BottomNavigationBadge;
import com.pangaea.quests.utils.BottomNavigationViewHelper;
import com.pangaea.quests.utils.PermissionUtils;
import com.pangaea.quests.utils.PixelConverter;
import com.pangaea.quests.utils.PreferenceUtils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import static com.pangaea.quests.top.createTab.step.SelectLocationMapFragment.LOCATION_PERMISSION_REQUEST_CODE;

public class TopLevelActivity extends AppCompatActivity {

    public static final String SELECTION_EXTRA = "selection";
    private static final String TAG = TopLevelActivity.class.getSimpleName();

    private TextView mTextMessage; // remove
    private boolean mPermissionDenied = false;
    private BottomNavigationView mNavigation;
    private int selection = 1;
    private Fragment activeFragment;
    private FragmentManager mFragmentManager;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.news:
                    activeFragment = new NewsFragment();
                    showFragment(activeFragment);
                    getSupportActionBar().setTitle("Newsfit");
                    getSupportActionBar().setElevation(PixelConverter.convertDpToPixel(4));
                    selection = 1;
                    return true;

                case R.id.navigation_list:

                    activeFragment = new TopQuestsFragment();
                    showFragment(activeFragment);
                    getSupportActionBar().setTitle("Quests");
                    getSupportActionBar().setElevation(0);
                    selection = 2;
                    return true;

                case R.id.navigation_create_quest:
                    Intent startIntent = new Intent(TopLevelActivity.this, CreateQuestActivity.class);
                    startActivity(startIntent);
                    return false;
                case R.id.navigation_notifications:
                    activeFragment = new NotificationFragment();
                    showFragment(activeFragment);
                    getSupportActionBar().setTitle("Notifications");
                    getSupportActionBar().setElevation(PixelConverter.convertDpToPixel(4));
                    selection = 4;
                    return true;
                case R.id.navigation_user_profile:
                    activeFragment = new UserProfileFragment();
                    showFragment(activeFragment);
                    getSupportActionBar().setTitle("Profile");
                    getSupportActionBar().setElevation(PixelConverter.convertDpToPixel(4));
                    selection = 5;
                    return true;

            }
            return false;
        }
    };

    private void showFragment(Fragment fragmentToShow) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.bottom_navigation_container, fragmentToShow).commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_level);

        User.getInstance().getUserId();
        User.getInstance().getUserToken();
        Log.e("user","id = "+User.getInstance().getUserId());
        Log.e("user","token = "+User.getInstance().getUserToken());

        mTextMessage = (TextView) findViewById(R.id.message);
        mNavigation = (BottomNavigationView) findViewById(R.id.navigation);

        BottomNavigationViewHelper.disableShiftMode(mNavigation);

        BottomNavigationBadge.setUpBadge(mNavigation, 10, this);

        mNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        mFragmentManager = getSupportFragmentManager();
        if (savedInstanceState != null) {
            selection = savedInstanceState.getInt(SELECTION_EXTRA);
        }
        if (selection == 2) {
            getSupportActionBar().setElevation(0);
        }
//        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(myToolbar);  //todo implement toolbar+elevation+no action bar theme
//        getSupportActionBar().setElevation(PixelConverter.convertDpToPixel(4));
        handleMyLocationPermission();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        loadAvatarImage(menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int selectedItemId = item.getItemId();
        if (selectedItemId == R.id.action_map) {
            Intent settingsIntent = new Intent(this, MapsActivity.class);
            startActivity(settingsIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void loadAvatarImage(Menu menu) {
        MenuItem userItem = menu.findItem(R.id.menu_user);
        View actionView = userItem.getActionView();

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                UserActivity.start(GroupsActivity.this);
                Toast.makeText(TopLevelActivity.this, "click", Toast.LENGTH_SHORT).show();
            }
        });

        final ImageView userImageView = actionView.findViewById(R.id.menu_user_image);
        String userPhotoUrl = PreferenceUtils.getUserPhotoUrl(this);

        Picasso.with(this).load(userPhotoUrl)
                .resize(64, 64)
//                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(userImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        Bitmap imageBitmap = ((BitmapDrawable) userImageView.getDrawable()).getBitmap();
                        RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(getResources(), imageBitmap);
                        imageDrawable.setCircular(true);
                        imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
                        userImageView.setImageDrawable(imageDrawable);
                    }

                    @Override
                    public void onError() {
                        userImageView.setImageResource(R.drawable.avatar_tmp);
                        //no need to set default, its already have set in xml
                    }
                });


    }


    public void handleMyLocationPermission() {
        if (ContextCompat.checkSelfPermission(QuestsApplication.get(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission((AppCompatActivity) this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
//            PermissionUtils.fragmentRequestPermission((AppCompatActivity) getActivity(),this, LOCATION_PERMISSION_REQUEST_CODE,
//                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
//                if (selectMamFragment != null) ;
//                selectMamFragment.enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.

            showMissingPermissionError();

        }
        mPermissionDenied = false;
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.

            showMissingPermissionError();

        }
        mPermissionDenied = false;
    }

    public void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(this.getSupportFragmentManager(), "dialog");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(SELECTION_EXTRA, selection);
        super.onSaveInstanceState(outState);
    }
}
