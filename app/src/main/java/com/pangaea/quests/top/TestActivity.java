package com.pangaea.quests.top;

import android.Manifest;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.maps.model.Marker;
import com.pangaea.quests.R;
import com.pangaea.quests.top.createTab.step.SelectLocationMapFragment;
import com.pangaea.quests.utils.PermissionUtils;

import static com.pangaea.quests.top.createTab.step.SelectLocationMapFragment.LOCATION_PERMISSION_REQUEST_CODE;

public class TestActivity extends AppCompatActivity {
    private SelectLocationMapFragment selectMamFragment;

    Marker save;
    private boolean mPermissionDenied = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        if (savedInstanceState == null) {
            selectMamFragment = SelectLocationMapFragment.newInstance("asd", "asd");

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.map_container, selectMamFragment, "map_select_fragment").commit();

        } else {
            selectMamFragment = (SelectLocationMapFragment) getSupportFragmentManager().findFragmentByTag("map_select_fragment");
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            if (selectMamFragment != null) ;
            selectMamFragment.enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }

    public boolean isPermissionDenied() {
        return mPermissionDenied;
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            if (selectMamFragment != null) {
                selectMamFragment.showMissingPermissionError();
            }
            mPermissionDenied = false;
        }
    }
}
