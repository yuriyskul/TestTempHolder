package com.pangaea.quests.model;

/**
 * Created by yuriy on 2/14/2018.
 */

public class Constants {
    public static final int UNDEFINED_CATEGORY = 0;
    public static final int SPORT_CATEGORY = 1;
    public static final int SOCIAL_CATEGORY = 2;
    public static final int TRAVEL_CATEGORY = 3;
    public static final int PARTY_CATEGORY = 4;
    public static final int BUSINESS_CATEGORY = 5;
    public static final int[] categories =
            {SPORT_CATEGORY, SOCIAL_CATEGORY, TRAVEL_CATEGORY, PARTY_CATEGORY, BUSINESS_CATEGORY};
}
