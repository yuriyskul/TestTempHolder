package com.pangaea.quests.top.createTab.step;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

//import com.pangaea.quests.R;
import com.pangaea.quests.R;
import com.pangaea.quests.model.Quest;
import com.pangaea.quests.top.createTab.CreateQuestActivity;
import com.pangaea.quests.utils.CategoryUtils;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import static com.pangaea.quests.model.Constants.BUSINESS_CATEGORY;
import static com.pangaea.quests.model.Constants.PARTY_CATEGORY;
import static com.pangaea.quests.model.Constants.SOCIAL_CATEGORY;
import static com.pangaea.quests.model.Constants.SPORT_CATEGORY;
import static com.pangaea.quests.model.Constants.TRAVEL_CATEGORY;
import static com.pangaea.quests.model.Constants.UNDEFINED_CATEGORY;

/**
 * A simple {@link Fragment} subclass.
 */
public class DescriptionStepFragment extends Fragment implements Step, View.OnClickListener {

    private TextView mCategoryValueTV;
    private ImageView mCategoryIV;
    private Button mSetCategoryBtn;
    private View popupCategoriView;

    private int categoryId = -1;
    private EditText mTitleET;
    private EditText mDescriptionET;

    public DescriptionStepFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_description_step, container, false);

        mTitleET = rootView.findViewById(R.id.et_step_title);

        mDescriptionET = rootView.findViewById(R.id.et_step_description);
        mDescriptionET.setOnEditorActionListener(new DoneOnEditorActionListener());
        mDescriptionET.setImeOptions(EditorInfo.IME_ACTION_DONE);
        mDescriptionET.setRawInputType(InputType.TYPE_CLASS_TEXT);

        mSetCategoryBtn = rootView.findViewById(R.id.fragment_description_step_set_type_btn);
        mSetCategoryBtn.setOnClickListener(this);
        popupCategoriView = rootView.findViewById(R.id.fragment_description_popup_categories_layout);
        popupCategoriView.setVisibility(View.GONE);

        mCategoryIV = rootView.findViewById(R.id.fragment_description_step_category_icon);
        mCategoryIV.setVisibility(View.INVISIBLE);
        mCategoryValueTV = rootView.findViewById(R.id.fragment_description_step_category_value);
        mCategoryValueTV.setVisibility(View.INVISIBLE);
        initCategoryButtons(rootView);

//        mSetCategoryBtn.setOnClickListener();
        return rootView;

    }


    private void handleOnCategoryClick(int categoryId) {
        ((CreateQuestActivity) getActivity()).getTempQuest().setCategory(categoryId);
        String category = CategoryUtils.getCategory(categoryId);
        mCategoryValueTV.setText(category);
        CategoryUtils.setImageByCategory(mCategoryIV, categoryId, getActivity());
        popupCategoriView.setVisibility(View.INVISIBLE);
        mCategoryIV.setVisibility(View.VISIBLE);
        mCategoryValueTV.setVisibility(View.VISIBLE);
        this.categoryId = categoryId;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fragment_description_step_set_type_btn:
                slideInFromLeft(getActivity(), popupCategoriView);
                break;
            case R.id.select_category_sport:
                handleOnCategoryClick(SPORT_CATEGORY);
                break;

            case R.id.select_category_social:
                handleOnCategoryClick(SOCIAL_CATEGORY);
                break;
            case R.id.select_category_travel:
                handleOnCategoryClick(TRAVEL_CATEGORY);
                break;
            case R.id.select_category_party:
                handleOnCategoryClick(PARTY_CATEGORY);
                break;
            case R.id.select_category_business:
                handleOnCategoryClick(BUSINESS_CATEGORY);
                break;

            case R.id.select_category_undefined:
                handleOnCategoryClick(UNDEFINED_CATEGORY);
                break;
        }
    }

    private void initCategoryButtons(View root) {
        Button sportBtn = root.findViewById(R.id.select_category_sport);
        sportBtn.setOnClickListener(this);
        Button socialBtn = root.findViewById(R.id.select_category_social);
        socialBtn.setOnClickListener(this);
        Button travelBtn = root.findViewById(R.id.select_category_travel);
        travelBtn.setOnClickListener(this);
        Button partyBtn = root.findViewById(R.id.select_category_party);
        partyBtn.setOnClickListener(this);
        Button businessBtn = root.findViewById(R.id.select_category_business);
        businessBtn.setOnClickListener(this);
        Button otherBtn = root.findViewById(R.id.select_category_undefined);
        otherBtn.setOnClickListener(this);

    }

    //todo move to the base class
    private VerificationError notifyUserWithError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
        return new VerificationError(msg);
    }
    //todo move to the base class
    private Quest getTempQuest() {
        return ((CreateQuestActivity) getActivity()).getTempQuest();
    }


    @Nullable
    @Override
    public VerificationError verifyStep() {
        String description = mDescriptionET.getText().toString().trim();
        String title = mTitleET.getText().toString().trim();
        if (TextUtils.isEmpty(title)) {
            return notifyUserWithError("title is empty");
        }
        if (TextUtils.isEmpty(description)) {
            String msg = "description is empty";
            return notifyUserWithError(msg);

        }
        if (categoryId == -1) {
            return notifyUserWithError("select quest category");
        }

        Quest quest = getTempQuest();
        quest.setCategory(categoryId);
        quest.setDescription(description);
        quest.setTitle(title);
        return null;
    }

    @Override
    public void onSelected() {
        Toast.makeText(getActivity(), "onSelected", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }


    public void slideInFromLeft(Context context, View view) {
        runSimpleAnimation(context, view, R.anim.slide_from_left);
    }

    private void runSimpleAnimation(Context context, final View view, int animationId) {
        Animation animation = AnimationUtils.loadAnimation(
                context, animationId);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(animation);
    }



   public class DoneOnEditorActionListener implements TextView.OnEditorActionListener {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return true;
            }
            return false;
        }
    }
}
