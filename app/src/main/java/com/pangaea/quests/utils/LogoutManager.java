package com.pangaea.quests.utils;

import android.content.Context;

/**
 * Created by yuriy on 2/8/2018.
 */

public class LogoutManager {

    public static void deleteUserData(Context context) {
        PreferenceUtils.clearAllPreferences(context);

    }
}
