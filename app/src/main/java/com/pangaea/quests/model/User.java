package com.pangaea.quests.model;

import android.content.Context;

import com.pangaea.quests.QuestsApplication;
import com.pangaea.quests.utils.PreferenceUtils;

/**
 * Created by yuriy on 2/8/2018.
 */

public class User {

    private static User instance = new User();

    private User() {
        initUser();
    }

    private String userId;
    private String userToken;
    private String userProfilePhotoUrl;

    public static User getInstance() {
        if (instance == null) {
            instance = new User(); //change to creating from current storage
        }
        return instance;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
        PreferenceUtils.saveUserId(QuestsApplication.get(), userId);
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
        PreferenceUtils.saveUserToken(QuestsApplication.get(), userToken);
    }

    public String getUserProfilePhotoUrl() {
        return userProfilePhotoUrl;
    }

    public void setUserProfilePhotoUrl(String userProfilePhotoUrl) {
        this.userProfilePhotoUrl = userProfilePhotoUrl;
        PreferenceUtils.saveUserPhotoUrl(QuestsApplication.get(), userProfilePhotoUrl);
    }

    public static void clearUser() {
        PreferenceUtils.clearAllPreferences(QuestsApplication.get());
        instance = null;
    }

    public void initUser() {
        Context context = QuestsApplication.get();
        userId = PreferenceUtils.getUserId(context);
        userToken = PreferenceUtils.getUserToken(context);
        userProfilePhotoUrl = PreferenceUtils.getUserPhotoUrl(context);
    }
}
