package com.pangaea.quests.top.questTab;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.pangaea.quests.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CalendarFragment extends Fragment implements Filterable {


    public CalendarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_calendar, container, false);
    }

    @Override
    public void applyFilter(int filterType) {
        Toast.makeText(getContext(), FilterMap.getFilterNameById(filterType)+ "has been applied", Toast.LENGTH_SHORT).show();
    }
}
