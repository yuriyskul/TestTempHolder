package com.pangaea.quests.networkClient.responseObject;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yuriy on 2/21/2018.
 */

public class UserData {

    public UserData() {
    }


    @SerializedName("id")
    @Expose
    private long userId;

    @SerializedName("first_name")
    @Expose
    private String firstName;


    @SerializedName("last_name")
    @Expose
    private String lastName;

    @SerializedName("middle_name")
    @Expose
    private String middleName;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("profile_image")
    @Expose
    private String profileImage;

    @SerializedName("gender")
    @Expose
    private String gender;

//    @SerializedName("groups")
//    @Expose
//    private String [] groups;

    @SerializedName("birth_date")
    @Expose
    private String birthDate;

    @SerializedName("description")
    @Expose
    private String description;

    public long getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getEmail() {
        return email;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public String getGender() {
        return gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "UserData{" +
                "userId=" + userId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", email='" + email + '\'' +
                ", profileImage='" + profileImage + '\'' +
                ", gender='" + gender + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
