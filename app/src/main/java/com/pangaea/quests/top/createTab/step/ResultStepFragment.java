package com.pangaea.quests.top.createTab.step;


import android.content.DialogInterface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.pangaea.quests.R;
import com.pangaea.quests.model.Quest;
import com.pangaea.quests.top.createTab.CreateQuestActivity;
import com.pangaea.quests.utils.CategoryUtils;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class ResultStepFragment extends Fragment implements Step {

    Button doneBtn;

    String pattern = "EEEEE MMMMM yyyy HH:mm";

    TextView titleTV;
    TextView descriptionTV;

    ImageView categoryIV;
    TextView categoryNameTV;

    TextView startDateTV;
    TextView endDateTV;

    public ResultStepFragment() {
        // Required empty public constructor
    }

    public void showAlertDialogButtonClicked() {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialogInterface) {
//
//                saveQuest();
//            }
//        });
        builder.setTitle("Publish new quest");
        builder.setMessage("You have created a new quest, by pressing done button quest is going to be published");

        // add the buttons
        builder.setPositiveButton("Publish", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                saveQuest();
                dialogInterface.dismiss();
                getActivity().finish();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View root = inflater.inflate(R.layout.fragment_result_step, container, false);
        doneBtn = root.findViewById(R.id.fragment_result_step_done);
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlertDialogButtonClicked();
            }
        });

        CreateQuestActivity createQuestActivity = (CreateQuestActivity) getActivity();
        Quest tempQuest = createQuestActivity.getTempQuest();

        categoryIV = root.findViewById(R.id.result_fragment_category_imageview);
        categoryNameTV = root.findViewById(R.id.result_fragment_category_value);
        descriptionTV = root.findViewById(R.id.result_fragment_description);


        startDateTV = root.findViewById(R.id.result_fragment_start_date);
        endDateTV = root.findViewById(R.id.result_fragment_end_date);


        titleTV = root.findViewById(R.id.result_fragment_title);
        titleTV.setText(tempQuest.getTitle());
        descriptionTV.setText(tempQuest.getDescription());
        int categoryId = tempQuest.getCategory();
        CategoryUtils.setImageByCategory(categoryIV, categoryId, createQuestActivity);
        String selectedCategoryName = CategoryUtils.getCategory(categoryId);
        categoryNameTV.setText(selectedCategoryName);

        LatLng tempLocation = tempQuest.getLocation();
        String address = getCompleteAddressString(tempLocation.latitude, tempLocation.longitude);

        TextView locationTV = root.findViewById(R.id.fragment_result_step_locationName);
        locationTV.setText(address);
        return root;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        showAlertDialogButtonClicked();
        return null;
    }

    @Override
    public void onSelected() {
        CreateQuestActivity createQuestActivity = (CreateQuestActivity) getActivity();
        Quest tempQuest = createQuestActivity.getTempQuest();
        Date startDate = tempQuest.getStartTime();
        Date endDate = tempQuest.getStartTime();
        String startDateStr = SimpleDateFormat.getDateTimeInstance().format(startDate);
        String endDateStr = SimpleDateFormat.getDateTimeInstance().format(endDate);
        startDateTV.setText(startDateStr);
        endDateTV.setText(endDateStr);
    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }


    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("loction", strReturnedAddress.toString());
            } else {
                Log.w("loction", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("loction", "Canont get Address!");
        }
        return strAdd;
    }

    //todo save quest to db
    private void saveQuest() {
        Toast.makeText(getContext(), "SAVING QUEST TO DB", Toast.LENGTH_SHORT).show();
    }


}
