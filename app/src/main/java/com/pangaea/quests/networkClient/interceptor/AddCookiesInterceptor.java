package com.pangaea.quests.networkClient.interceptor;

import android.preference.PreferenceManager;
import android.util.Log;

import com.pangaea.quests.QuestsApplication;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.prefs.Preferences;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by yuriy on 2/22/2018.
 */

public class AddCookiesInterceptor implements Interceptor {
    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();
        HashSet<String> preferences = Cookie.getCookies(QuestsApplication.get());
        for (String cookie : preferences) {
            builder.addHeader("Cookie", cookie);
            Log.e("OkHttp", "Adding Header: " + cookie); // This is done so I know which headers are being added; this interceptor is used after the normal logging of OkHttp
        }
        return chain.proceed(builder.build());
    }

}
