package com.pangaea.quests.networkClient;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.pangaea.quests.networkClient.interceptor.AddCookiesInterceptor;
import com.pangaea.quests.networkClient.interceptor.ReceivedCookiesInterceptor;

import java.io.IOException;
import java.net.CookieHandler;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by yuriy on 2/21/2018.
 */

public class ServiceGenerator {

    public static final String DOMAIN = "http://questsapp.com";  // quest domain //todo
    public static final String API_V0 = DOMAIN + "/api/v0/";




//    private static OkHttpClient.Builder okHttpClient = new OkHttpClient().newBuilder();
//            .connectTimeout(60 * 5, TimeUnit.SECONDS)
//            .readTimeout(60 * 5, TimeUnit.SECONDS)
//            .writeTimeout(60 * 5, TimeUnit.SECONDS);

//    static {
//        okHttpClient.interceptors().add(new AddCookiesInterceptor());
//        okHttpClient.interceptors().add(new ReceivedCookiesInterceptor());
//    }



//
//    private static Retrofit.Builder builder =
//            new Retrofit.Builder()
//                    .client(okHttpClient.build())
//                    .baseUrl(API_V0)
//                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                    .addConverterFactory(GsonConverterFactory.create());
//
//    private static Retrofit retrofit = builder.build();

    private static Retrofit generateRetrofit(){
        OkHttpClient.Builder okHttpClient_t = new OkHttpClient().newBuilder();
        okHttpClient_t.interceptors().add(new AddCookiesInterceptor());
        okHttpClient_t.interceptors().add(new ReceivedCookiesInterceptor());

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .client(okHttpClient_t.build())
                        .baseUrl(API_V0)
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create());
        return builder.build();
    }

    public static <S> S createService(
            Class<S> serviceClass) {



//        return retrofit.create(serviceClass);
        return generateRetrofit().create(serviceClass);
    }



}
