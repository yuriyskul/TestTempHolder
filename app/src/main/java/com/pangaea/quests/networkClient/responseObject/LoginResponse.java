package com.pangaea.quests.networkClient.responseObject;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yuriy on 2/21/2018.
 */

public class LoginResponse {
    public LoginResponse() {
    }

    @SerializedName("auth_token")
    @Expose
    private String authToken;

    @SerializedName("new_user")
    @Expose
    private boolean newUser;

    public String getAuthToken() {
        return authToken;
    }

    public boolean isNewUser() {
        return newUser;
    }
}
