package com.pangaea.quests;

import android.app.Application;

import com.facebook.FacebookSdk;

/**
 * Created by yuriy on 2/7/2018.
 */

public class QuestsApplication extends Application {
    private static QuestsApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
//        FacebookSdk.sdkInitialize(getApplicationContext()); no need any more


//        Fresco.initialize(this);
    }

    public static QuestsApplication get() {
        return instance;
    }
}
