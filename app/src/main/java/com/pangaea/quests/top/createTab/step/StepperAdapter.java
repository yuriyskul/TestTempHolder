package com.pangaea.quests.top.createTab.step;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.pangaea.quests.model.Quest;
import com.pangaea.quests.top.createTab.step.date.DateStepFragment;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

/**
 * Created by yuriy on 2/12/2018.
 */

public class StepperAdapter extends AbstractFragmentStepAdapter {
    public static final int DESCRIPTION_STEP = 0;
    public static final int LOCATION_STEP = 1;
    public static final int DATE_STEP = 2;
    public static final int RESULT_STEP = 3;

    private Quest quest;

    public StepperAdapter(FragmentManager fm, Context context) {
        super(fm, context);
    }

    @Override
    public Step createStep(int position) {
        switch (position) {
            case DESCRIPTION_STEP:
                final DescriptionStepFragment descriptionStep = new DescriptionStepFragment();
                Bundle b = new Bundle();
//                b.putInt(CURRENT_STEP_POSITION_KEY, position);
//                step.setArguments(b);
                return descriptionStep;
            case LOCATION_STEP:
                final SelectLocationMapFragment locationStepFragment = new SelectLocationMapFragment();
//                Bundle b = new Bundle();
//                b.putInt(CURRENT_STEP_POSITION_KEY, position);
//                step.setArguments(b);
                return locationStepFragment;
            case DATE_STEP:
                final DateStepFragment dateStepFragment = new DateStepFragment();
                Log.e("check","datestep click");
                return dateStepFragment;
            case RESULT_STEP:
                final ResultStepFragment resultStepFragment = new ResultStepFragment();
                Log.e("check","resultStep click");
                return resultStepFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        //Override this method to set Step title for the Tabs, not necessary for other stepper types
        return new StepViewModel.Builder(context)
                .setTitle("Что за тайтл!") //can be a CharSequence instead
                .create();
    }
}
