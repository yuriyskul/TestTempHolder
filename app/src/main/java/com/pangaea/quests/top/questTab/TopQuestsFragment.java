package com.pangaea.quests.top.questTab;


import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.pangaea.quests.R;
import com.pangaea.quests.utils.PixelConverter;

import static com.pangaea.quests.top.questTab.FilterMap.NO_FILTER;

/**
 * A simple {@link Fragment} subclass.
 */
public class TopQuestsFragment extends Fragment implements View.OnClickListener {

    int filter = 0;

    FloatingActionsMenu menuMultipleActions;
    FloatingActionButton fab_1;
    FloatingActionButton fab_2;
    FloatingActionButton fab_3;

    QuestsPagerAdapter adapterViewPager;

    public TopQuestsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_quests, container, false);
        ViewPager vpPager = (ViewPager) root.findViewById(R.id.viewpager);
        adapterViewPager = new QuestsPagerAdapter(getChildFragmentManager());
        vpPager.setAdapter(adapterViewPager);
        vpPager.setOffscreenPageLimit(3);

        initMenuFab(root);

        return root;
    }

    private void initMenuFab(View root) {
        menuMultipleActions = (FloatingActionsMenu) root.findViewById(R.id.multiple_actions);
        fab_1 = root.findViewById(R.id.action_filter_1);
        fab_1.setOnClickListener(this);
        fab_2 = root.findViewById(R.id.action_filter_2);
        fab_2.setOnClickListener(this);
        fab_3 = root.findViewById(R.id.action_filter_3);
        fab_3.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.action_filter_1:
                applyFilter(filter, FilterMap.FAVORITE_FILTER);
                menuMultipleActions.collapse();
                break;
            case R.id.action_filter_2:
                applyFilter(filter, FilterMap.JOINED_FILTER);
                menuMultipleActions.collapse();
                break;
            case R.id.action_filter_3:
                applyFilter(filter, FilterMap.MINE_FILTER);
                menuMultipleActions.collapse();
                break;
        }
    }


    public void applyFilter(int oldFilter, int newFilter) {
        Filterable currentFragment = (Filterable) adapterViewPager.getCurrentFragment();
        if (oldFilter == newFilter) {
            adapterViewPager.setCurrentFilterType(NO_FILTER); //notify activity
            filter = NO_FILTER;                                 //notify adapter
            currentFragment.applyFilter(NO_FILTER);             //notify current fragment
        } else {
            adapterViewPager.setCurrentFilterType(newFilter); //notify activity
            filter = newFilter;                                 //notify adapter
            currentFragment.applyFilter(newFilter);
        }
    }
}
