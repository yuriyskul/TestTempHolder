package com.pangaea.quests.model;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.pangaea.quests.QuestsApplication;

/**
 * Created by yuriy on 2/22/2018.
 */

public class UserInfo {

    public static final String USER_ID_PREF = "user_id_pref";
    public static final String USER_FIRST_NAME_PREF = "user_first_name_pref";
    public static final String USER_LAST_NAME_PREF = "user_last_name_pref";
    public static final String USER_MIDDLE_NAME_PREF = "user_middle_name_pref";
    public static final String USER_EMAIL_PREF = "user_email_pref";
    public static final String USER_PROFILE_IMAGE_PREF = "user_profile_img_pref";
    public static final String USER_GENDER_PREF = "user_gender_pref";
    public static final String USER_BIRTHDAY_PREF = "user_birthday_pref";
    public static final String USER_DESCRIPTION_PREF = "user_description_pref";

    private long userId;

    private String firstName;

    private String lastName;

    private String middleName;

    private String email;

    private String profileImage;

    private String gender;

    private String birthDate;

    private String description;

    public static void saveCurrentUserInfo(UserInfo newUserInfo) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(QuestsApplication.get());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(USER_ID_PREF, newUserInfo.userId);
        editor.putString(USER_FIRST_NAME_PREF, newUserInfo.firstName);
        editor.putString(USER_LAST_NAME_PREF, newUserInfo.lastName);
        editor.putString(USER_MIDDLE_NAME_PREF, newUserInfo.middleName);
        editor.putString(USER_EMAIL_PREF, newUserInfo.email);
        editor.putString(USER_PROFILE_IMAGE_PREF, newUserInfo.profileImage);
        editor.putString(USER_GENDER_PREF, newUserInfo.gender);
        editor.putString(USER_BIRTHDAY_PREF, newUserInfo.birthDate);
        editor.putString(USER_DESCRIPTION_PREF, newUserInfo.description);
        editor.apply();
    }

    public static UserInfo extractUserInfo() {
        UserInfo userInfo = new UserInfo();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(QuestsApplication.get());
        userInfo.userId = preferences.getLong(USER_ID_PREF, 0);
        userInfo.firstName = preferences.getString(USER_FIRST_NAME_PREF, null);
        userInfo.lastName = preferences.getString(USER_LAST_NAME_PREF, null);
        userInfo.middleName = preferences.getString(USER_MIDDLE_NAME_PREF, null);
        userInfo.email = preferences.getString(USER_EMAIL_PREF, null);
        userInfo.profileImage = preferences.getString(USER_PROFILE_IMAGE_PREF, null);
        userInfo.gender = preferences.getString(USER_GENDER_PREF, null);
        userInfo.birthDate = preferences.getString(USER_BIRTHDAY_PREF, null);
        userInfo.description = preferences.getString(USER_DESCRIPTION_PREF, null);
        return userInfo;
    }


    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "userId=" + userId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", email='" + email + '\'' +
                ", profileImage='" + profileImage + '\'' +
                ", gender='" + gender + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
