package com.pangaea.quests.utils;

import android.content.res.Resources;
import android.util.DisplayMetrics;

/**
 * Created by yuriy on 2/13/2018.
 */

public class PixelConverter {
    private PixelConverter() {
    }

    public static float convertPixelsToDp(float px) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float dp = px / (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT); //160f
        return Math.round(dp);
    }

    public static float convertDpToPixel(float dp) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT); //160f
        return Math.round(px);
    }
}
