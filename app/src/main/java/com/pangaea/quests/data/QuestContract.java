package com.pangaea.quests.data;

import android.provider.BaseColumns;



public class QuestContract {
    public static final class QuestEntry implements BaseColumns {
        public static final String TABLE_NAME = "quests";

        public static final String COLUMN_TITLE = "title"; //save as a string
        public static final String COLUMN_DESCRIPTION = "description"; // save as a string
        public static final String COLUMN_CATEGORY = "category";       //save as int
        public static final String COLUMN_START_TIME = "start_time";    //save as long
        public static final String COLUMN_END_TIME = "end_time";        //save as long
        public static final String COLUMN_LATITUDE = "latitude";        //save as double
        public static final String COLUMN_LONGITUDE = "longitude";       //save as double


        public static final String COLUMN_PRIORITY = "priority";
    }
}
