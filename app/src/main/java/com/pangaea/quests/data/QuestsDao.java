package com.pangaea.quests.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import com.pangaea.quests.model.Quest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class QuestsDao {
    private SQLiteDatabase database;
    private QuestDbHelper dbHelper;

    public QuestsDao(Context context) {
        dbHelper = new QuestDbHelper(context);
        // Gets the data repository in write mode
        database = dbHelper.getWritableDatabase();

    }

    public void close() {
        dbHelper.close();
    }

    /**
     * insert a text report item to the textreport database table
     *
     * @param "textReport"
     * @return the row ID of the newly inserted row, or -1 if an error occurred
     */

    public long addReport(Quest quest) {
        // Create a new map of values, where column names are the keys
        ContentValues cv = new ContentValues();
        cv.put(QuestContract.QuestEntry.COLUMN_TITLE, quest.getTitle());
        cv.put(QuestContract.QuestEntry.COLUMN_DESCRIPTION, quest.getDescription());
        cv.put(QuestContract.QuestEntry.COLUMN_CATEGORY, quest.getCategory());

        String startFormattedDate = getFormattedDate(quest.getStartTime());
        String endFormattedDate = getFormattedDate(quest.getEndTime());


        cv.put(QuestContract.QuestEntry.COLUMN_START_TIME, startFormattedDate);
        cv.put(QuestContract.QuestEntry.COLUMN_END_TIME, endFormattedDate);

        cv.put(QuestContract.QuestEntry.COLUMN_LATITUDE, quest.getLocation().longitude);
        cv.put(QuestContract.QuestEntry.COLUMN_LONGITUDE, quest.getLocation().latitude);


        // Insert the new row, returning the primary key value of the new row
        return database.insert(QuestContract.QuestEntry.TABLE_NAME, null, cv);
    }


    /**
     * @return all text reports as a List
     */
    public List<Quest> getAllQuests() {
        List<Quest> textReports = new ArrayList<>();

        Cursor cursor =
                database.query(QuestContract.QuestEntry.TABLE_NAME, null, null, null, null, null,
                        null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Quest quest = cursorToQuest(cursor);
            textReports.add(quest);
            Log.e("openhelper",quest.getTitle());
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return textReports;
    }

    /**
     * *
     *
     * @param cursor the cursor row
     * @return a TextReport
     */
    private Quest cursorToQuest(Cursor cursor) {
        Quest q = new Quest();
        long id = cursor.getLong(cursor.getColumnIndex(QuestContract.QuestEntry._ID));
        q.set_ID(id);
        q.setTitle(cursor.getString(cursor.getColumnIndex(QuestContract.QuestEntry.COLUMN_TITLE)));
//        q.setDatetime(cursor.getLong(cursor.getColumnIndex(DBtables.TextReport.COLUMN_DATETIME)));
//        q.setLongitude(cursor.getDouble(cursor.getColumnIndex(DBtables.TextReport.COLUMN_LONGITUDE)));
//        q.setLatitude(cursor.getDouble(cursor.getColumnIndex(DBtables.TextReport.COLUMN_LATITUDE)));
//        // here we convert int to boolean
//        tr.setIsreported(cursor.getInt(cursor.getColumnIndex(DBtables.TextReport.COLUMN_ISREPOETED)) > 0);
        return q;
    }


    public static String getFormattedDate(Date date) {

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return dateFormat.format(date);
    }
}
