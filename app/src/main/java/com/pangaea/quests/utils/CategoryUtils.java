package com.pangaea.quests.utils;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import com.pangaea.quests.R;

import static com.pangaea.quests.model.Constants.BUSINESS_CATEGORY;
import static com.pangaea.quests.model.Constants.PARTY_CATEGORY;
import static com.pangaea.quests.model.Constants.SOCIAL_CATEGORY;
import static com.pangaea.quests.model.Constants.SPORT_CATEGORY;
import static com.pangaea.quests.model.Constants.TRAVEL_CATEGORY;
import static com.pangaea.quests.model.Constants.UNDEFINED_CATEGORY;

/**
 * Created by yuriy on 2/14/2018.
 */

public class CategoryUtils {
    public static void setImageByCategory(ImageView imageView, int category, Context context) {
        imageView.setImageDrawable(ContextCompat.getDrawable(context, getImageResIdByCategory(category)));
    }

    public static String getCategory(int categoryId) {
        switch (categoryId) {
            case SPORT_CATEGORY:
                return "Sport";
            case SOCIAL_CATEGORY:
                return "Social";
            case TRAVEL_CATEGORY:
                return "Travel";
            case PARTY_CATEGORY:
                return "Party";
            case BUSINESS_CATEGORY:
                return "Business";
            case UNDEFINED_CATEGORY:
                return "Other";
            default:
                return "Other";
        }
    }

    private static int getImageResIdByCategory(int category) {
        switch (category) {
            case SPORT_CATEGORY:
                return R.drawable.ic_category_sport_24dp;
            case SOCIAL_CATEGORY:
                return R.drawable.ic_category_social_24dp;
            case TRAVEL_CATEGORY:
                return R.drawable.ic_category_travel_24dp;
            case PARTY_CATEGORY:
                return R.drawable.ic_category_party_24dp;
            case BUSINESS_CATEGORY:
                return R.drawable.ic_category_business_black_24dp;
            case UNDEFINED_CATEGORY:
                return R.drawable.ic_category_undefined_24dp;
            default:
                return R.drawable.ic_category_undefined_24dp;
        }
    }
}
