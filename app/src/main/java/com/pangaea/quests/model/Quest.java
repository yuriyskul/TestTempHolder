package com.pangaea.quests.model;

import com.google.android.gms.maps.model.LatLng;

import java.util.Date;
import java.util.List;


public class Quest {

    public Quest() {
    }

    private long _ID;
    private LatLng location;
    private String ownerId;

    private String title;
    private String description;
    private Date startTime;
    private Date endTime;

    private int type;
    private int category;

    private int participantsMaxValue = -1;

    public long get_ID() {
        return _ID;
    }

    public void set_ID(long _ID) {
        this._ID = _ID;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getParticipantsMaxValue() {
        return participantsMaxValue;
    }

    public void setParticipantsMaxValue(int participantsMaxValue) {
        this.participantsMaxValue = participantsMaxValue;
    }


    //    private List<String> participantsList;
//
//    private int participantsMinValue;
//
//    private int ageMin;
//    private int ageMax;
}
