package com.pangaea.quests.top.createTab.step.date;

/**
 * Created by yuriy on 2/13/2018.
 */

public interface DateCallBack {
    void handleDateCallback(int year, int month, int day, String typeOfCallback);

    void handleTimeCallback(int hourOfDay, int minute, String typeOfCallback);
}
