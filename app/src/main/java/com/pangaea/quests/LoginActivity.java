package com.pangaea.quests;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.widget.ToolTipPopup;
import com.pangaea.quests.editUser.UserEditActivity;
import com.pangaea.quests.model.User;
import com.pangaea.quests.model.UserInfo;
import com.pangaea.quests.networkClient.ErrorHandler;
import com.pangaea.quests.networkClient.responseObject.LoginResponse;
import com.pangaea.quests.networkClient.responseObject.UserData;
import com.pangaea.quests.viewModel.LoginViewModel;

public class LoginActivity extends AppCompatActivity {
    private static final String EMAIL = "email";
    private static final String PUBLIC_PROFILE = "public_profile";
    private static final String USER_FRIENDS = "user_friends";
    private static final String USER_BIRTHDAY = "user_birthday";

    LoginButton faceBookButton;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (isUserLoggedIn()) {

            UserEditActivity.startEditUserActivity(LoginActivity.this);  //todo quck start editActivity
            finish(); // no need to show this one anyMore                                //todo quck start editActivity
            startTopLevelActivity();
        }

        setContentView(R.layout.activity_login);


        AppEventsLogger.activateApp(getApplication());
        faceBookButton = (LoginButton) findViewById(R.id.login_button);
        faceBookButton.setReadPermissions(EMAIL, PUBLIC_PROFILE, USER_FRIENDS, USER_BIRTHDAY);   //todo define all permissions we need for app
        callbackManager = CallbackManager.Factory.create();
        faceBookButton.setToolTipStyle(ToolTipPopup.Style.BLUE);

        faceBookButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            private ProfileTracker mProfileTracker;

            @Override
            public void onSuccess(final LoginResult loginResult) {
                faceBookButton.setVisibility(View.VISIBLE);

                Toast.makeText(LoginActivity.this, "On onSuccess", Toast.LENGTH_SHORT).show();

                if (Profile.getCurrentProfile() == null) {
                    mProfileTracker = new ProfileTracker() {
                        @Override
                        protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
//                            Log.e("facebook - profile", "onCurrentProfileChanged");
//                            Log.e("facebook - profile", currentProfile.getFirstName());
//                            String profilePictureLink = Profile.getCurrentProfile().getProfilePictureUri(500, 500).toString();
//                            Log.e("facebook - profile", profilePictureLink);
                            doLoginToServer(loginResult);
                            mProfileTracker.stopTracking();
                        }
                    };
                    // no need to call startTracking() on mProfileTracker
                    // because it is called by its constructor, internally.
                } else {
                    doLoginToServer(loginResult);
//                    Profile profile = Profile.getCurrentProfile();
//                    Log.e("facebook - profile", profile.getFirstName());
                }

            }

            @Override
            public void onCancel() {
                faceBookButton.setVisibility(View.VISIBLE);
                Toast.makeText(LoginActivity.this, "onCancel()", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                faceBookButton.setVisibility(View.VISIBLE);
                Log.d("mylog", "Successful: " + exception.toString());
            }
        });

        subscribeToLoginLiveData();
        subscribeToUserLiveData();
    }

    private void doLoginToServer(LoginResult loginResult) {
        Profile p = Profile.getCurrentProfile();
        Log.e("facebook - profile", "onCurrentProfileChanged");
        Log.e("facebook - profile", p.getFirstName());
        Log.e("facebook - profile", "profile id " + p.getId());
        Log.e("facebook - profile", "login result id " + loginResult.getAccessToken().getUserId());
        Log.e("facebook - profile", "first name " + p.getFirstName());
        String profilePictureLink = Profile.getCurrentProfile().getProfilePictureUri(500, 500).toString();
        Log.e("facebook - profile", profilePictureLink);
        Log.e("facebook - profile", "name " + p.getName());


        String authToken = loginResult.getAccessToken().getToken();
        String userId = loginResult.getAccessToken().getUserId();
        //faceBook saving userData
        User user = User.getInstance();
        user.setUserToken(authToken);
        user.setUserId(userId);
        Log.e("Login", "token= " + loginResult.getAccessToken().getToken());
        Log.e("Login", "userId= " + loginResult.getAccessToken().getUserId());
//                String profilePictureLink = Profile.getCurrentProfile().getProfilePictureUri(500, 500).toString();
        Log.e("Login", "profile URL= " + loginResult.getAccessToken().getUserId());


        LoginViewModel.getViewModel(LoginActivity.this).runLoginSubscription(userId, authToken);

        //                startTopLevelActivity();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

//    public void userEditUserActivity(Context packageContext) {
//        Intent startIntent = new Intent(LoginActivity.this, UserEditActivity.class);
//        startActivity(startIntent);
//        finish();
//    }

    public void startTopLevelActivity() {
//        Intent startIntent = new Intent(LoginActivity.this, TopLevelActivity.class);
//        startActivity(startIntent);
//        finish();
    }

    private boolean isUserLoggedIn() {
        return (AccessToken.getCurrentAccessToken() != null || User.getInstance().getUserToken() != null);
    }


    private void subscribeToLoginLiveData() {
        LoginViewModel.getViewModel(LoginActivity.this).getLoginResponseLiveData().observe(this, new android.arch.lifecycle.Observer<LoginViewModel.LiveDataLoginContainer>() {
            @Override
            public void onChanged(@Nullable LoginViewModel.LiveDataLoginContainer liveDataLoginContainer) {
                if (liveDataLoginContainer == null) {
                    Log.e("onChanged", "or first start or clearedData");
                    return;
                }
                if (liveDataLoginContainer.isNoErrorResponse()) {
                    LoginResponse resultResp = liveDataLoginContainer.getLoginResponse();
                    String token = resultResp.getAuthToken();
                    boolean isNewUser = resultResp.isNewUser();

                    Log.e("onChanged", "noError" + "token:" + token + " is new user = " + isNewUser);

//                    if (isNewUser) {
//                        //start userEditActivity and finish this one
//
//                    } else {
//
                    LoginViewModel.getViewModel(LoginActivity.this).runUserDataSubscription();
//                    }
                    //save to SharedPref, go to start activity, call finish()
                } else {

                    ErrorHandler.logError(liveDataLoginContainer.getThrowable());
                }
                LoginViewModel.getViewModel(LoginActivity.this).clearLoginLifeData();
            }
        });
    }

    private void subscribeToUserLiveData() {
        LoginViewModel.getViewModel(LoginActivity.this).getUserResponseLiveData().observe(this, new android.arch.lifecycle.Observer<LoginViewModel.LiveDataUserContainer>() {
            @Override
            public void onChanged(@Nullable LoginViewModel.LiveDataUserContainer liveDataUserContainer) {
                if (liveDataUserContainer == null) {
                    Log.e("onChanged", "user no data null ,mock");
                    return;
                }
                if (liveDataUserContainer.isNoErrorResponse()) {
                    UserData userData = liveDataUserContainer.getUserData();
//                    String userName = userData.getFirstName();

                    Log.e("onChanged", "noError user" + userData.toString());

                    saveUserToUserInfo(userData);
                    // details has been got - need to finish current activity and send user to editInfoActivity
                    UserEditActivity.startEditUserActivity(LoginActivity.this);
                    finish(); // no need to show this one anyMore
                } else {

                    ErrorHandler.logError(liveDataUserContainer.getThrowable());
                }
                LoginViewModel.getViewModel(LoginActivity.this).clearUserLifeData();
            }
        });
    }

    //todo move this method to UserData
    private static void saveUserToUserInfo(UserData userData) {
        UserInfo userInfo = new UserInfo();
        userInfo.setUserId(userData.getUserId());
        userInfo.setFirstName(userData.getFirstName());
        userInfo.setLastName(userData.getLastName());
        userInfo.setMiddleName(userData.getMiddleName());
        userInfo.setEmail(userData.getEmail());
        userInfo.setProfileImage(userData.getProfileImage());
        userInfo.setGender(userData.getGender());
        userInfo.setDescription(userData.getDescription());
        userInfo.setDescription(userData.getBirthDate());

        UserInfo.saveCurrentUserInfo(userInfo);
    }
}
