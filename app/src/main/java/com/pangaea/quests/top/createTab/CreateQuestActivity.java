package com.pangaea.quests.top.createTab;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.pangaea.quests.R;
import com.pangaea.quests.model.Quest;
import com.pangaea.quests.top.createTab.step.StepperAdapter;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.stepstone.stepper.viewmodel.StepViewModel;

public class CreateQuestActivity extends AppCompatActivity implements StepperLayout.StepperListener {

    private StepperLayout mStepperLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_quest);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mStepperLayout = (StepperLayout) findViewById(R.id.stepperLayout);
        StepperAdapter adapter = new StepperAdapter(getSupportFragmentManager(), this);
        mStepperLayout.setAdapter(adapter);
//        adapter.
        StepViewModel model = adapter.getViewModel(0);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    public NewQuestViewModel getViewModel() {
        return ViewModelProviders.of(this).get(NewQuestViewModel.class);
    }

    public Quest getTempQuest() {
        return getViewModel().getTempQuest();
    }

    @Override
    public void onCompleted(View completeButton) {

    }

    @Override
    public void onError(VerificationError verificationError) {

    }

    @Override
    public void onStepSelected(int newStepPosition) {

    }

    @Override
    public void onReturn() {

    }
}
