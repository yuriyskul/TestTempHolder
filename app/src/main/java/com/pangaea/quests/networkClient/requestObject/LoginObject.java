package com.pangaea.quests.networkClient.requestObject;


import android.util.Log;

public class LoginObject {

    private String userID;
    private String accessToken;

    public LoginObject(String userID, String accessToken) {
        this.userID = userID;
        this.accessToken = accessToken;
        Log.e("LogObj", this.toString());
        toString();
    }

    @Override
    public String toString() {
        return "LoginObject{" +
                "userID='" + userID + '\'' +
                ", accessToken='" + accessToken + '\'' +
                '}';
    }
}
