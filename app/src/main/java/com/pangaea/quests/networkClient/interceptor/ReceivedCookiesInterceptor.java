package com.pangaea.quests.networkClient.interceptor;

import android.preference.PreferenceManager;
import android.util.Log;

import com.pangaea.quests.QuestsApplication;

import java.io.IOException;
import java.util.HashSet;

import okhttp3.Interceptor;
import okhttp3.Response;

import static com.pangaea.quests.networkClient.interceptor.Cookie.COOKIES_KEY;

/**
 * Created by yuriy on 2/22/2018.
 */

//public class ReceivedCookiesInterceptor implements Interceptor {
//    @Override
//    public Response intercept(Chain chain) throws IOException {
//        Response originalResponse = chain.proceed(chain.request());
//        if (!originalResponse.headers("Set-Cookie").isEmpty()) {
//            HashSet<String> cookies = new HashSet<>();
//            for (String header : originalResponse.headers("Set-Cookie")) {
//                cookies.add(header);
//                Log.e("OkHttp", "resp Adding Header: " + header);
//            }
//            Cookie.setCookies(QuestsApplication.get(), cookies);
//        }
//        return originalResponse;
//    }
//}

public class ReceivedCookiesInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Response originalResponse = chain.proceed(chain.request());

        if (!originalResponse.headers("Set-Cookie").isEmpty()) {
            HashSet<String> cookies = new HashSet<>();

            for (String header : originalResponse.headers("Set-Cookie")) {
                cookies.add(header);
                Log.e("OkHttp", "reciever: " + header); // This is done so I know which headers are being added; this interceptor is used after the normal logging of OkHttp

            }

            PreferenceManager.getDefaultSharedPreferences(QuestsApplication.get()).edit()
                    .putStringSet(COOKIES_KEY, cookies)
                    .apply();
        }

        return originalResponse;
    }
}
