package com.pangaea.quests.top.createTab.step.date;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.format.DateFormat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.pangaea.quests.R;
import com.pangaea.quests.model.Quest;
import com.pangaea.quests.top.createTab.CreateQuestActivity;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class DateStepFragment extends Fragment implements Step, View.OnClickListener, DateCallBack {

    private static final String DAY_FORMAT_PATTERN = "yyyy-MM-dd"; //todo correct localisation
    private static final String TIME_FORMAT_PATTERN = "HH:mm";
    private static final int UNDEFINED = -1;
    public static final String CALLBACK_FROM_START_PICKER = "start_callback";
    public static final String CALLBACK_FROM_END_PICKER = "end_callback";

    int startYear = UNDEFINED;
    int startMonth = UNDEFINED;
    int startDay = UNDEFINED;
    int startHoursOfDay = UNDEFINED;
    int startMinutes = UNDEFINED;

    int endYear = UNDEFINED;
    int endMonth = UNDEFINED;
    int endDay = UNDEFINED;
    int endHoursOfDay = UNDEFINED;
    int endMinutes = UNDEFINED;


    public DateStepFragment() {
        // Required empty public constructor
    }


    private TextView mStartDateTv;
    private TextView mStartTimeTv;

    private TextView mEndDateTv;
    private TextView mEndTimeTv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_date_step, container, false);

        mStartDateTv = rootView.findViewById(R.id.tv_set_start_date);
        CardView startDateCv = rootView.findViewById(R.id.cv_pick_start_date);
        startDateCv.setOnClickListener(this);

        mStartTimeTv = rootView.findViewById(R.id.tv_set_start_time);
        CardView startTimeCv = rootView.findViewById(R.id.cv_pick_start_time);
        startTimeCv.setOnClickListener(this);

        mEndDateTv = rootView.findViewById(R.id.tv_set_end_date);
        CardView endDateCv = rootView.findViewById(R.id.cv_pick_end_date);
        endDateCv.setOnClickListener(this);

        mEndTimeTv = rootView.findViewById(R.id.tv_set_end_time);
        CardView endTimeCv = rootView.findViewById(R.id.cv_pick_end_time);
        endTimeCv.setOnClickListener(this);

        return rootView;
    }

    public void showDatePickerDialog(View v, String typeCallback) {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.setCallBack(this);
        newFragment.setType(typeCallback);
        newFragment.show(getChildFragmentManager(), "datePicker");
    }

    public void showTimePickerDialog(View v, String type) {
        TimePickerFragment newFragment = new TimePickerFragment();
        newFragment.setCallBack(this);
        newFragment.setType(type);

        newFragment.show(getChildFragmentManager(), "timePicker");
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {

        if (startYear == UNDEFINED || startMonth == UNDEFINED || startDay == UNDEFINED) {
            return notifyUserWithError("pickup start date");
        }
        if (startHoursOfDay == UNDEFINED || startMinutes == UNDEFINED) {
            return notifyUserWithError("pickup start time");
        }
        if (endYear == UNDEFINED || endMonth == UNDEFINED || endDay == UNDEFINED) {
            return notifyUserWithError("pickup end date");
        }
        if (endHoursOfDay == UNDEFINED || endMinutes == UNDEFINED) {
            return notifyUserWithError("pickup end time");
        }

        Date resultStartDate = generateDate(startYear, startMonth, startDay, startHoursOfDay, startMinutes);
        Date resultEndDate = generateDate(endYear, endMonth, endDay, endHoursOfDay, endMinutes);
        String startDateStr = SimpleDateFormat.getDateTimeInstance().format(resultStartDate);
        String endDateStr = SimpleDateFormat.getDateTimeInstance().format(resultEndDate);

        if (resultStartDate.after(resultEndDate)) {
            return notifyUserWithError("end date should be after start date");
        }
        Quest tempQuest = getTempQuest();
        tempQuest.setStartTime(resultStartDate);
        tempQuest.setEndTime(resultEndDate);
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cv_pick_start_date:
//                mActiveDateDisplay = mStartDateTv;
                showDatePickerDialog(view, CALLBACK_FROM_START_PICKER);
                break;
            case R.id.cv_pick_start_time:
//                mActiveDateDisplay = mStartTimeTv;
                showTimePickerDialog(view, CALLBACK_FROM_START_PICKER);
                break;
            case R.id.cv_pick_end_date:
//                mActiveDateDisplay = mEndDateTv;
                showDatePickerDialog(view, CALLBACK_FROM_END_PICKER);
                break;
            case R.id.cv_pick_end_time:
//                mActiveDateDisplay = mEndTimeTv;
                showTimePickerDialog(view, CALLBACK_FROM_END_PICKER);
                break;
            default:
                throw new UnsupportedOperationException("unknown view");
        }
    }

    @Override
    public void handleDateCallback(int year, int month, int day, String typeOfCallback) {
        Date date = generateDate(year, month, day, 0, 0);

        String result = SimpleDateFormat.getDateInstance().format(date);


        if (typeOfCallback.equals(CALLBACK_FROM_START_PICKER)) {

            startYear = year;
            startMonth = month;
            startDay = day;
            mStartDateTv.setText(result);
        }
        if (typeOfCallback.equals(CALLBACK_FROM_END_PICKER)) {

            endYear = year;
            endMonth = month;
            endDay = day;
            mEndDateTv.setText(result);
        }
    }

    @Override
    public void handleTimeCallback(int hourOfDay, int minute, String typeOfCallback) {
        Date date = generateDate(0, 0, 0, hourOfDay, minute);
        String formattedTime = SimpleDateFormat.getTimeInstance().format(date);

        if (typeOfCallback.equals(CALLBACK_FROM_START_PICKER)) {

            startHoursOfDay = hourOfDay;
            startMinutes = minute;
            mStartTimeTv.setText(formattedTime);
        }
        if (typeOfCallback.equals(CALLBACK_FROM_END_PICKER)) {
            endHoursOfDay = hourOfDay;
            endMinutes = minute;
            mEndTimeTv.setText(formattedTime);
        }
    }


    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        private DateCallBack callBack;
        private String type;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker

            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            if (callBack != null) {
                callBack.handleDateCallback(year, month, day, type);
            }
        }

        public void setType(String type) {
            this.type = type;
        }

        public void setCallBack(DateCallBack callBack) {
            this.callBack = callBack;
        }

        @Override
        public void onDetach() {
            super.onDetach();
            callBack = null;
        }
    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {


        private DateCallBack callBack;
        private String type;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);
            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user
            String result = hourOfDay + ":" + minute;

            if (callBack != null) {
                callBack.handleTimeCallback(hourOfDay, minute, type);
            }
        }

        public void setType(String type) {
            this.type = type;
        }

        public DateCallBack getCallBack() {
            return callBack;
        }

        public void setCallBack(DateCallBack callBack) {
            this.callBack = callBack;
        }

        @Override
        public void onDetach() {
            super.onDetach();
            callBack = null;
        }

    }

    public Date generateDate(int year, int month, int day, int hours, int minutes) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, hours);
        cal.set(Calendar.MINUTE, minutes);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    //todo move to the base class
    private VerificationError notifyUserWithError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
        return new VerificationError(msg);
    }

    //todo move to the base class
    private Quest getTempQuest() {
        return ((CreateQuestActivity) getActivity()).getTempQuest();
    }

}
