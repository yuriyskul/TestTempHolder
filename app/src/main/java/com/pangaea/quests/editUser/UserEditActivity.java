package com.pangaea.quests.editUser;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.pangaea.quests.LoginActivity;
import com.pangaea.quests.R;
import com.pangaea.quests.model.UserInfo;

public class UserEditActivity extends AppCompatActivity {
    TextView infoTv;
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_edit);
        infoTv = findViewById(R.id.editUserActivity_info_tv);

        UserInfo userInfo = UserInfo.extractUserInfo();
        infoTv.setText(userInfo.toString());

//        btn.findViewById(R.id.activity_user_edit_btn_update);
    }


    public static void startEditUserActivity(Context packageContext) {
        Intent startIntent = new Intent(packageContext, UserEditActivity.class);
        packageContext.startActivity(startIntent);
    }

    public void onClickUpdate(View view) {
        Toast.makeText(this, "on update", Toast.LENGTH_SHORT).show();
    }

    private void sendUpdate(){


    }
}
