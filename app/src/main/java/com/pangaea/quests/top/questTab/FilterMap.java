package com.pangaea.quests.top.questTab;

/**
 * Created by yuriy on 2/16/2018.
 */

public class FilterMap {
    public static final String FILTER_EXTRA = "filter_extra";

    public static final int NO_FILTER = 0;
    public static final int FAVORITE_FILTER = 1;
    public static final int JOINED_FILTER = 2;
    public static final int MINE_FILTER = 3;

    public static String getFilterNameById(int filterId) {
        switch (filterId) {
            case NO_FILTER:
                return "no";
            case FAVORITE_FILTER:
                return "favorite";
            case JOINED_FILTER:
                return "joined";
            case MINE_FILTER:
                return "mine";
        }
        return "no";
    }

    ;
}
