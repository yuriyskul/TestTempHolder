package com.pangaea.quests.top.createTab;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.pangaea.quests.model.Quest;

import java.util.List;

/**
 * Created by yuriy on 2/14/2018.
 */

public class NewQuestViewModel extends ViewModel {

    private Quest tempQuest;

    public Quest getTempQuest() {
        if (tempQuest == null) {
            tempQuest = new Quest();
        }
        return tempQuest;
    }

}
