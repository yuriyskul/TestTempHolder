package com.pangaea.quests.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by yuriy on 2/17/2018.
 */

public class QuestDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "QuestsDb.db";

    private static final int VERSION = 1;

    private static final String TEXT_TYPE = " TEXT";
    private static final String REAL_TYPE = " REAL";
    private static final String REAL_TYPE_NOT_NULL = " REAL NOT NULL";
    private static final String NUMERIC_TYPE = " NUMERIC";
    private static final String COMMA_SEP = ",";


    public QuestDbHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String CREATE_TABLE = "CREATE TABLE " + QuestContract.QuestEntry.TABLE_NAME + " (" +
                QuestContract.QuestEntry._ID + " INTEGER PRIMARY KEY, " +
                QuestContract.QuestEntry.COLUMN_TITLE + " TEXT NOT NULL, " +
                QuestContract.QuestEntry.COLUMN_DESCRIPTION + " TEXT NOT NULL, " +
                QuestContract.QuestEntry.COLUMN_CATEGORY + " INTEGER NOT NULL, " +
                QuestContract.QuestEntry.COLUMN_LATITUDE + " INTEGER NOT NULL, " +
                QuestContract.QuestEntry.COLUMN_LATITUDE + REAL_TYPE_NOT_NULL + "," +
                QuestContract.QuestEntry.COLUMN_LONGITUDE + REAL_TYPE_NOT_NULL + "," +
                QuestContract.QuestEntry.COLUMN_START_TIME + NUMERIC_TYPE + "," +
                QuestContract.QuestEntry.COLUMN_END_TIME + NUMERIC_TYPE + ");";

        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + QuestContract.QuestEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
