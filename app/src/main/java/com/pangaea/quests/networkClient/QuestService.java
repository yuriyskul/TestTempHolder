package com.pangaea.quests.networkClient;

import com.pangaea.quests.networkClient.requestObject.LoginObject;
import com.pangaea.quests.networkClient.responseObject.LoginResponse;
import com.pangaea.quests.networkClient.responseObject.UserData;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by yuriy on 2/21/2018.
 */

public interface QuestService {

    @POST("login")
    Observable<LoginResponse> login(@Body LoginObject loginObject);


    @GET("users/details/")
    Observable<UserData> getUserDetails();



    //update user details
    @PUT("users/{userId}/")
    Observable<UserData> updateUserDetails(@Path("userId") String userId,@Body UserData requestUserData);


    @GET("users/details/")
    Call<UserData> getUserDataF();

//    @GET("/users/{userId}")
//    Call<UserData> getUserDataF(@Path("userId") String userId);

}
