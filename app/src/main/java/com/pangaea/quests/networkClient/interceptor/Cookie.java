package com.pangaea.quests.networkClient.interceptor;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.pangaea.quests.utils.PreferenceUtils;

import java.util.HashSet;

/**
 * Created by yuriy on 2/22/2018.
 */

public class Cookie {
    public static final String COOKIES_KEY = "cookies";
    public static HashSet<String> getCookies(Context context) {
        SharedPreferences mcpPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return (HashSet<String>) mcpPreferences.getStringSet(COOKIES_KEY, new HashSet<String>());
    }

    public static void setCookies(Context context, HashSet<String> cookies) {
        SharedPreferences mcpPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = mcpPreferences.edit();
         editor.putStringSet(COOKIES_KEY, cookies).apply();
    }
}
