package com.pangaea.quests.top.questTab;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.pangaea.quests.R;

import static com.pangaea.quests.top.questTab.FilterMap.FILTER_EXTRA;

/**
 * A simple {@link Fragment} subclass.
 */
public class QuestListFragment extends Fragment implements Filterable {
    CardView filterCardView;
    TextView filterTextView;


    public QuestListFragment() {
        // Required empty public constructor
    }

    public static QuestListFragment newInstance(int filterType) {

        Bundle args = new Bundle();
        args.putInt(FILTER_EXTRA, filterType);
        QuestListFragment fragment = new QuestListFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_quest_list, container, false);
        filterCardView = root.findViewById(R.id.fragment_quest_list_filter_cardview);
        filterTextView = root.findViewById(R.id.fragment_quest_list_filter_value);

        int filterType = getArguments().getInt(FILTER_EXTRA);
        applyFilter(filterType);

        return root;
    }

    @Override
    public void applyFilter(int filterType) {

        String filter = FilterMap.getFilterNameById(filterType);
        String text = filter + " filter has been applied";
        Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
        filterTextView.setText(text);
    }
}
