package com.pangaea.quests.top.createTab.step;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pangaea.quests.QuestsApplication;
import com.pangaea.quests.R;
import com.pangaea.quests.model.Quest;
import com.pangaea.quests.top.createTab.CreateQuestActivity;
import com.pangaea.quests.utils.PermissionUtils;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import static com.facebook.FacebookSdk.getApplicationContext;


public class SelectLocationMapFragment extends Fragment implements View.OnClickListener,
        OnMapReadyCallback, GoogleMap.OnMapClickListener, Step {

    private FloatingActionButton myLocation;
    private FloatingActionButton saveMyLocation;
    private Button testBtn;
    //    private boolean mPermissionDenied = false;
    private GoogleMap mMap;
    Marker tempMarker;
    private FloatingActionButton mFab;
    PlaceAutocompleteFragment placeAutoComplete;

    public static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    public SelectLocationMapFragment() {
        // Required empty public constructor
    }


    public static SelectLocationMapFragment newInstance(String param1, String param2) {
        SelectLocationMapFragment fragment = new SelectLocationMapFragment();
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_select_location_map, container, false);
        myLocation = rootView.findViewById(R.id.btn_my_current_location);
        saveMyLocation = rootView.findViewById(R.id.btn_save_location);
        if (tempMarker != null) {
            saveMyLocation.setVisibility(View.VISIBLE);

        }
        myLocation.setOnClickListener(this);
        saveMyLocation.setOnClickListener(this);
        testBtn = rootView.findViewById(R.id.btn_test);

        SupportPlaceAutocompleteFragment autocompleteFragment = new SupportPlaceAutocompleteFragment();
        android.support.v4.app.FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment_content, autocompleteFragment);
        ft.commit();
        Log.e("sss", "" + (autocompleteFragment == null));
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                LatLng selectedPlace = place.getLatLng();
                showMarker(selectedPlace);
                Toast.makeText(getContext(), place.getName(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Toast.makeText(getContext(), "An error occurred: " + status, Toast.LENGTH_SHORT).show();
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        return rootView;
    }

//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }

    @Override
    public void onClick(View view) {
        int viewId = view.getId();
        switch (viewId) {
            case R.id.btn_my_current_location:
                Log.e("TST", "go to current loc");
                break;
            case R.id.btn_save_location:
                Log.e("TST", "Save Location");
                break;
            default:
                //do nothing
                ;
        }
    }

    private static int counter = 1;

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        Toast.makeText(getContext(), "map ready callback", Toast.LENGTH_SHORT).show();
        mMap = googleMap;
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(0, 0))
                .title("Marker"));
        googleMap.setMapType(1);
        googleMap.setOnMapClickListener(this);
        testBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (counter == 5) {
                    counter = 0;
                }
                counter++;
                googleMap.setMapType(counter);
            }
        });
//        googleMap.
        enableMyLocation();
    }

    public void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(QuestsApplication.get(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission((AppCompatActivity) getActivity(), LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
//            PermissionUtils.fragmentRequestPermission((AppCompatActivity) getActivity(),this, LOCATION_PERMISSION_REQUEST_CODE,
//                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }

    private void showMarker(LatLng latLng) {
        if (tempMarker != null) {
            tempMarker.remove();
        } else {
            Animation fabShow = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_show);
            fabShow.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    saveMyLocation.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animation animation) {

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            saveMyLocation.startAnimation(fabShow);
        }
        tempMarker = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title("Quest"));
        CameraUpdate update = CameraUpdateFactory.newLatLng(latLng);
//        mMap.moveCamera(update);
        mMap.animateCamera(update);

        updateTempQuest(latLng);

    }

    private void updateTempQuest(LatLng latLng) {
        getTempQuest().setLocation(latLng);
    }


    @Override
    public void onMapClick(LatLng latLng) {
        Toast.makeText(getActivity(), "marker click", Toast.LENGTH_SHORT).show();
        showMarker(latLng);
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        if (getTempQuest().getLocation() == null && tempMarker == null) {
            return notifyUserWithError("pickup quest location");
        }
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        if (getActivity().mPermissionDenied) {
//            // Permission was not granted, display error dialog.
//            showMissingPermissionError();
//            Toast.makeText(getActivity(), "mPermissionDenied", Toast.LENGTH_SHORT).show();
//            mPermissionDenied = false;
//        }
//    }

    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    public void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getActivity().getSupportFragmentManager(), "dialog");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Toast.makeText(getActivity(), "onReqPermresult", Toast.LENGTH_SHORT).show();
    }


    //todo move to the base class
    private VerificationError notifyUserWithError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
        return new VerificationError(msg);
    }

    //todo move to the base class
    private Quest getTempQuest() {
        return ((CreateQuestActivity) getActivity()).getTempQuest();
    }


}
