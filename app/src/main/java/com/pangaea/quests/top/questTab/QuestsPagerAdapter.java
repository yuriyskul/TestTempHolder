package com.pangaea.quests.top.questTab;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;


public class QuestsPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 3;

    private int currentFilterType = FilterMap.NO_FILTER;

    public QuestsPagerAdapter(FragmentManager fm) {
        super(fm);
    }
    private Fragment mCurrentFragment;

    public Fragment getCurrentFragment() {
        return mCurrentFragment;
    }

    public int getCurrentFilterType() {
        return currentFilterType;
    }

    public void setCurrentFilterType(int currentFilterType) {
        this.currentFilterType = currentFilterType;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
//                return FirstFragment.newInstance(0, "Page # 1");
                return QuestListFragment.newInstance(currentFilterType);
            case 1: // Fragment # 0 - This will show FirstFragment different title
                return new QuestsMapFragment();
            case 2: // Fragment # 0 - This will show FirstFragment different title
                return new CalendarFragment();
            default:
                return null;
        }
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
        if (getCurrentFragment() != object) {
            mCurrentFragment = ((Fragment) object);
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "List";
            case 1:
                return "Map";
            case 2:
                return "Calendar";
            default:
                return "List"; // unexpected case
        }

    }
}
